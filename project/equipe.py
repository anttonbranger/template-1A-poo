import pandas as pd
bd = pd.read_csv('./BDD/results.csv')

# On vérifie que le nom de l'equipe est une chaine de caractère
home_team = bd['home_team']
away_team = bd['away_team']
for i in range(len(home_team)):
    if not isinstance(home_team[i], str):
        raise TypeError("Le nom de l'equipe doit être une chaine"
                        "de caractère")
    if not isinstance(away_team[i], str):
        raise TypeError("Le nom de l'equipe doit être une chaine"
                        "de caractère")


class Equipe:
    """
    La classe Equipe permet de créer un objet de type equipe.

    Paramètres:
    ----------
    nom : str
        Nom de l'equipe
    continent : str
        Continent de l'equipe
    """
    def __init__(self, nom):
        if not isinstance(nom, str):
            raise TypeError("Le nom de l'equipe doit être une chaine"
                            "de caractère")
        self.nom = nom

    def __repr__(self) -> str:
        return f"Equipe({self.nom})"
