from project.Statistique.statistique import Statistique
from abc import ABC
from datetime import date
from project.equipe import Equipe


class Serie(Statistique, ABC):
    '''Cette classe abstraite code la methode serie sans l'implémenter.'''
    def __init__(self, date_debut: date, date_fin: date):
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")
        self.date_debut = date_debut
        self.date_fin = date_fin

    def serie(self, equipe: Equipe) -> int:
        pass
