# Interface utilisateur

from datetime import date
from project.equipe import Equipe
from project.historique import Historique
from project.prediction import Prediction
from project.Statistique.butpour import ButPour
from project.Statistique.butcontre import ButContre
from project.Statistique.cleansheet import CleanSheet
from project.Statistique.invincibilite import Invincibilite
from project.Statistique.victoire import Victoire

# POUR AVOIR L'ISSU D'UN MATCH

# Renseigner les deux equipes dont on souhaite prédire l'issue d'un match.
# Attention le nom des équipes doit être en anglais et doit être une équipe
# international.

nom_equipe_1 = "Argentina"
nom_equipe_2 = "Germany"

equipe_1 = Equipe(nom_equipe_1)
equipe_2 = Equipe(nom_equipe_2)

# Renseigner les dates entre lesquelles vous souhaitez baser les prédictions.
# La date de début est la plus ancienne et la date de fin la plus recente.

annee_debut = 2020
mois_debut = 5
jour_debut = 5
annee_fin = 2024
mois_fin = 5
jour_fin = 5

date_debut = date(annee_debut, mois_debut, jour_debut)
date_fin = date(annee_fin, mois_fin, jour_fin)
historique = Historique(date_debut, date_fin)

prediction = Prediction(equipe_1, equipe_2, historique)

resultat_methode_simple = prediction.methode_simple()
resultat_methode_pondere = prediction.methode_pondere()
resultat_methode_recente = prediction.methode_recente()

print(resultat_methode_simple)
print(resultat_methode_pondere)
print(resultat_methode_recente)


# POUR AVOIR LES STATISTIQUES D'UNE EQUIPE

# Renseigner le nom de l'équipe dont on souhaite voir les statistiques.

nom_equipe = "Germany"

equipe = Equipe(nom_equipe)

# Renseigner les dates entre lesquelles vous souhaitez baser les prédictions.

annee_debut = 2020
mois_debut = 5
jour_debut = 5
annee_fin = 2024
mois_fin = 5
jour_fin = 5

date_debut = date(annee_debut, mois_debut, jour_debut)
date_fin = date(annee_fin, mois_fin, jour_fin)

victoire = Victoire(date_debut, date_fin)
invincibilite = Invincibilite(date_debut, date_fin)
cleansheet = CleanSheet(date_debut, date_fin)
butpour = ButPour(date_debut, date_fin)
butcontre = ButContre(date_debut, date_fin)

serie_victoire = victoire.victoire(equipe)
serie_invincibilite = invincibilite.invincibilite(equipe)
serie_cleansheet = cleansheet.cleansheet(equipe)
moyenne_butpour = butpour.ButPour(equipe)
moyenne_butcontre = butcontre.ButContre(equipe)

print(serie_victoire)
print(serie_invincibilite)
print(serie_cleansheet)
print(moyenne_butpour)
print(moyenne_butcontre)
