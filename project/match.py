from project.equipe import Equipe
from project.score import Score
from datetime import date
import pandas as pd
df = pd.read_csv("./BDD/results.csv")


class Match:
    """Cette classe représente un match.

    Un match est caractérisé par deux équipes, une domicile et une extérieur.
    Ainsi qu'une date et un tournoi.

    Parameters
    ----------
        equipe_dom (Equipe): première équipe
        equipe_ext (Equipe): deuxième équipe
        date_match (date): date du match
        tournoi (str): si le match est amical ou si il a été joué dans un
        tournoi

    Exemples
    --------
    >>> equipe1 = Equipe("England")
    >>> equipe2 = Equipe("Scotland")
    >>> match1 = Match(equipe1, equipe2, df["Date"][2], "Friendly")
    >>> score1 = Score(df["home_score"][2], df["away_score"][2])
    >>> match1.gagnant(score1)
    "England"
    >>> match2 = Match(equipe2, equipe1, df["Date"][1], "Friendly")
    >>> score2 = Score(df["home_score"][1], df["away_score"][1])
    >>> match2.gagnant(score2)
    "Match nul"
    >>> match1.get_score()
    Score(4, 2)

    """

    def __init__(self, equipe_dom: Equipe, equipe_ext: Equipe,
                 date_match, tournoi: str):
        """Constructeur de la classe.

        Parameters
        ----------
            equipe_dom (Equipe): première équipe
            equipe_ext (Equipe): deuxième équipe
            date (date): date du match
            tournoi (str): _description_
        """
        if not isinstance(equipe_dom, Equipe):
            raise TypeError("L'equipe doit être une instance de Equipe")
        if not isinstance(equipe_ext, Equipe):
            raise TypeError("L'equipe doit être une instance de Equipe")
        if not isinstance(tournoi, str):
            raise TypeError("Le tournoi doit être une instance de str")
        self.equipe_dom = equipe_dom
        self.equipe_ext = equipe_ext
        self.date_match = date_match
        self.tournoi = tournoi

    def get_score(self) -> Score:
        """Renvoie le score du match."""
        df_copie = df
        date_matchs = df_copie["date"]
        n = len(date_matchs)
        for i in range(n):
            if date.fromisoformat(date_matchs[i]) == self.date_match:
                if df_copie["home_team"][i] == self.equipe_dom:
                    score = Score(df_copie["home_score"][i],
                                  df_copie["away_score"][i])
                    return score.__repr__()

    def gagnant(self) -> str:
        """Renvoie le gagnant du match.

        Parameters
        ----------
            score (Score): le score du match

        Returns
        -------
            str: le gagnant du match

        """
        score = self.get_score()
        if score.score_dom > score.score_ext:
            return self.equipe_dom.__repr__()
        elif score.score_dom < score.score_ext:
            return self.equipe_ext.__repr__()
        else:
            return "Match nul"
