from project.equipe import Equipe
import pandas as pd
import re
import pytest


df = pd.read_csv('./BDD/results.csv')


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'nom', 1}, TypeError, (
            "Le nom de l'equipe doit être une chaine de caractère"
        ))
    ]
)
def test_match_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Equipe(**kwargs)
