from project.Statistique.serie import Serie
from project.equipe import Equipe
from datetime import date
from project.historique import Historique
from project.match import Match

import pandas as pd

bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class CleanSheet(Serie):
    '''Cette classe permet de calculer le nombre de cleansheet consécutif
    d'une équipe depuis le dernier match.

    Exemples
    --------

    >>> equipe1 = Equipe("England")
    >>> date_debut1 = date(1872,11,30)
    >>> date_fin1 = date(1876,03,04)
    >>> cleansheet1 = CleanSheet(date_debut1, date_fin1)
    >>> cleansheet1.cleansheet(equipe1)
    0

    '''
    def __init__(self, date_debut: date, date_fin: date):
        super().__init__(date_debut, date_fin)
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")

    def cleansheet(self, equipe: Equipe) -> float:
        '''Fonction calculant le nombre de cleansheet consécutif d'une équipe.

        C'est - à - dire le nombre de matchs consécutifs où l'équipe n'a pas
        pris de but.

        Parameters
        ----------
        equipe : Equipe
            equipe dont on calcule le nombre de cleansheet consécutif

        Returns
        -------
        float
            le nombre de cleansheet consécutif de l'equipe
        '''
        serie = 0
        historique = Historique(self.date_debut, self.date_fin)
        liste_match = historique.get_historique_seul(matches, equipe)
        k = len(liste_match)
        while k > 0:
            if liste_match[k-1].equipe_dom == equipe and \
                 liste_match[k-1].score_ext == 0:
                serie += 1
            elif liste_match[k-1].equipe_ext == equipe and \
                 liste_match[k-1].score_dom == 0:
                serie += 1
            k -= 1
        return serie
