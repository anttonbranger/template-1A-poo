from project.Statistique.serie import Serie
from project.equipe import Equipe
from datetime import date
from project.historique import Historique
from project.match import Match

import pandas as pd

bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class Invincibilite(Serie):
    '''Cette classe calcule la série d'invincibilité d'une équipe.

    C'est-à-dire le nombre de victoire ou de match nul consécutif de l'equipe
    depuis le dernier match joué par cette équipe.

    Parameters
    ----------
    date_debut : date
        date de debut de la serie
    date_fin : date
        date de fin de la serie

    Examples
    --------
    >>> equipe1 = Equipe("England")
    >>> equipe2 = Equipe("Scotland")
    >>> date_debut1 = date(1872,11,30)
    >>> date_fin1 = date(1876,03,04)
    >>> invincibilite1 = Invincibilite(date_debut1, date_fin1)
    >>> invincibilite1.invincibilite(equipe1)
    0
    >>> invincibilite1.invincibilite(equipe2)
    3
    '''

    def __init__(self, date_debut: date, date_fin: date):
        super().__init__(date_debut, date_fin)
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")

    def invincibilite(self, equipe: Equipe) -> float:
        '''Fonction calculant le nombre de victoire ou de match nul
        consécutif de l'equipe.

        Parameters
        ----------
        equipe : Equipe
            equipe dont on calcule le nombre de victoire ou de match nul
            consécutif

        Returns
        -------
        int
            le nombre de victoire ou de match nul consécutif de l'equipe
        '''
        serie = 0
        historique = Historique(self.date_debut, self.date_fin)
        liste_match = historique.get_historique_seul(matches, equipe)
        k = len(liste_match)
        while k > 0 and (liste_match[k-1].gagnant() == equipe.nom or
                         liste_match[k-1].gagnant() == "Match nul"):
            serie += 1
            k -= 1
        return serie
