from project.score import Score
import pandas as pd
import pytest
import re


df = pd.read_csv('./BDD/results.csv')


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'score_dom': "France"}, TypeError, (
            "Le score_dom doit être une instance de int"
        )),
        ({'score_ext': [2, 4]}, TypeError, (
            "Le score_ext doit être une instance de int"
        ))
    ]
)
def test_score_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Score(**kwargs)
