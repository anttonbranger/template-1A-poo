from project.Statistique.moyenne import Moyenne
from datetime import date
from project.equipe import Equipe
from project.historique import Historique
from project.match import Match

import pandas as pd

bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class ButPour(Moyenne):
    '''Cette classe permet de calculer en moyenne combien de but une équipe
    marque par match entre date_debut et date_fin.

    Exemples
    --------
    >>> equipe = Equipe("England")
    >>> date_debut = date(1872, 11, 30)
    >>> date_fin = date(1876, 03, 04)
    >>> butpour = ButPour(date_debut, date_fin)
    >>> round(butpour.ButPour(equipe), 1)
    1.4

    '''

    def __init__(self, date_debut: date, date_fin: date):
        super().__init__(date_debut, date_fin)
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")

    def ButPour(self, equipe: Equipe) -> float:
        '''Fonction calculant le nombre de but marqué par match d'une équipe.

        Parameters
        ----------
        equipe : Equipe
            equipe dont on calcule le nombre moyen de but marqué par match

        Returns
        -------
        float
            le nombre moyen de but marqué par match de l'equipe
        '''
        historique = Historique(self.date_debut, self.date_fin)
        liste_match = historique.get_historique_seul(matches, equipe)
        n = len(liste_match)
        somme = 0
        for i in range(n):
            if liste_match[i].equipe_dom == equipe:
                somme += liste_match[i].score_dom
            elif liste_match[i].equipe_ext == equipe:
                somme += liste_match[i].score_ext
        return somme/n
