from project.prediction import Prediction
import pandas as pd
import re
import pytest


df = pd.read_csv('./BDD/results.csv')


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'equipe_dom': "France"}, TypeError, (
            "L'equipe doit être une instance de Equipe")),
        ({'equipe_ext': ["Germany"]}, TypeError, (
            "L'equipe doit être une instance de Equipe")),

    ]
)
def test_prediction_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Prediction(**kwargs)
