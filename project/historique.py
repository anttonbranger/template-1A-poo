from project.match import Match
from datetime import date
import pandas as pd
from project.equipe import Equipe


bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class Historique():
    """Cette classe représente un historique des matchs precedent.

    L'historique  est caractérisé par 2 dates: une de debut et une de fin .

    Examples:
    ---------
    >>> date_debut = date(1872, 11, 30)
    >>> date_fin = date(1876, 03, 22)
    >>> historique1 = Historique(date_debut, date_fin)
    >>> equipe1 = Equipe("Scotland")
    >>> equipe2 = Equipe("England")
    >>> match1 = Match(equipe1, equipe2, df["Date"][2], "Friendly")
    >>> match2 = Match(equipe2, equipe1, df["Date"][3], "Friendly")
    >>> match3 = Match(equipe2, equipe1, df["Date"][4], "Friendly")
    >>> match4 = Match(equipe2, equipe1, df["Date"][5], "Friendly")
    >>> match5 = Match(equipe2, equipe1, df["Date"][6], "Friendly")
    >>> matches = [match1, match2, match3, match4, match5]
    >>> historique1.get_historique(matches, equipe1, equipe2)
    [match1, match2, match3, match4, match5]
    >>> historique1.get_historique_seule(matches, equipe1)
    [match1, match2, match3, match4, match5]
    """
    def __init__(self, date_debut: date, date_fin: date):
        """Constructeur de la classe.

        Parameters
        ----------
            date_debut (date): date de debut de l historique
            date_fin (date): date de fin  de l historique
        """
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")
        self.date_debut = date_debut
        self.date_fin = date_fin

    def get_historique(self, matches, equipe1, equipe2):
        if not isinstance(equipe1, str) and isinstance(equipe2, str):
            return TypeError('Equipe doit etre une chaîne de caractère')
        resultat = []
        for i in range(len(matches)):
            m = matches[i]
            if self.date_debut < date.fromisoformat(m.date_match)\
               and date.fromisoformat(m.date_match) < self.date_fin:
                if m.equipe_dom == equipe1 and m.equipe_ext == equipe2:
                    resultat.append(m)
                elif m.equipe_dom == equipe2 and m.equipe_ext == equipe1:
                    resultat.append(m)
        return resultat

    def get_historique_seul(self, matches, equipe1):
        resultat_seul = []
        for i in range(len(matches)):
            m = matches[i]
            if self.date_debut < date.fromisoformat(m.date_match)\
               and date.fromisoformat(m.date_match) < self.date_fin:
                if m.equipe_dom == equipe1:
                    resultat_seul.append(m)
                elif m.equipe_ext == equipe1:
                    resultat_seul.append(m)
        return resultat_seul
