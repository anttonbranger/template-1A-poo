from project.Statistique.serie import Serie
from project.equipe import Equipe
from datetime import date
from project.historique import Historique
from project.match import Match

import pandas as pd

bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class Victoire(Serie):
    '''Cette classe permet de calculer le nombre de victoire consécutive
    d'une équipe depuis le dernier match joué par cette équipe.

    Parameters
    ----------
    date_debut : date
        date de debut de la serie
    date_fin : date
        date de fin de la serie

    Exemples
    --------
    >>> equipe1 = Equipe("England")
    >>> equipe2 = Equipe("Scotland")
    >>> date_debut1 = date(1872,11,30)
    >>> date_fin1 = date(1876,03,04)
    >>> victoire1 = Victoire(date_debut1, date_fin1)
    >>> victoire1.victoire(equipe1)
    0
    >>> victoire1.victoire(equipe2)
    1

    '''
    def __init__(self, date_debut: date, date_fin: date):
        if not isinstance(date_debut, date):
            raise TypeError("La date de debut doit être une instance de date")
        if not isinstance(date_fin, date):
            raise TypeError("La date de fin doit être une instance de date")
        super().__init__(date_debut, date_fin)

    def victoire(self, equipe: Equipe) -> float:
        ''' Fonction calculant le nombre de victoire consécutive d'une équipe.
        Parameters
        ----------
        equipe : Equipe
            equipe dont on calcule le nombre de victoire conséutive

        Returns
        -------
        float
            le nombre de victoire conséutive de l'equipe'''
        if not isinstance(equipe, Equipe):
            raise TypeError("L'equipe doit être une instance de Equipe")
        serie = 0
        historique = Historique(self.date_debut, self.date_fin)
        liste_match = historique.get_historique_seul(matches, equipe)
        k = len(liste_match)
        while k > 0 and liste_match[k-1].gagnant() == equipe.nom:
            serie += 1
            k -= 1
        return serie
