from project.lieu import Lieu
import pandas as pd
import pytest
import re


df = pd.read_csv('./BDD/results.csv')


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'terrain': 1}, TypeError, (
            "Le terrain doit être une instance de str"
        )),
        ({'terrain': ['France']}, TypeError, (
            "Le terrain doit être une instance de str"
        ))
    ]
)
def test_match_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Lieu(**kwargs)
