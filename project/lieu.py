from project.match import Match
import pandas as pd
df = pd.read_csv("./BDD/results.csv")


class Lieu:
    """Classe qui permet de savoir si un match a été joué en terrain neutre.

    Parameters
    ----------
        terrain (str): terrain du match

    Exemples
    --------
    >>> lieu1 = Lieu(df["country"][2])
    >>> equipe1 = Equipe("England")
    >>> equipe2 = Equipe("Scotland")
    >>> match1 = Match(equipe1, equipe2, df["Date"][2], "Friendly")
    >>> lieu1.terrain_neutre(match1)
    False
    """
    def __init__(self, terrain: str):
        if not isinstance(terrain, str):
            raise TypeError("Le terrain doit être une instance de str")
        self.terrain = terrain

    def terrain_neutre(self, match: Match) -> bool:
        """Vérifie si le match a été joué en terrain neutre.

        Parameters
        ----------
            match : Match

        Returns
        -------
            bool: True si le terrain est neutre et False sinon.

        """
        if self.terrain != match.equipe_dom.nom:
            return True
        return False
