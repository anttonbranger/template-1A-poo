from abc import ABC
from project.equipe import Equipe


class Statistique(ABC):
    '''Cette classe abstraite code la méthode statistique sans l'implémenter.
    '''
    def stat(self, equipe: Equipe) -> float:
        pass
