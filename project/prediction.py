from project.equipe import Equipe
from project.historique import Historique
from project.match import Match
from datetime import date
import pandas as pd

bd = pd.read_csv('./BDD/results.csv')


bd_hist = bd[["date", "home_team", "away_team", "home_score", "away_score",
              "tournament"]]

matches = []
for i in range(len(bd_hist)):
    matches.append(Match(Equipe(bd_hist["home_team"][i]),
                         Equipe(bd_hist["away_team"][i]),
                         bd_hist["date"][i],
                         bd_hist["tournament"][i]))


class Prediction:
    '''Cette classe contient différentes méthodes permettant de prédire le
    résultat d'un match

    Exemples
    --------
    >>> equipe1 = Equipe("England")
    >>> equipe2 = Equipe("Scotland")
    >>> historique1 = Historique(date(1872, 11, 30), date(1876, 03, 22))
    >>> prediction1 = Prediction(equipe1, equipe2, historique1)
    >>> prediction1.methode_simple()
    Scotland
    >>> prediction1.methode_pondere()
    Scotland
    >>> prediction1.methode_recente()
    Scotland
    '''
    def __init__(self, equipe_1: Equipe, equipe_2: Equipe,
                 historique: Historique) -> None:
        if not isinstance(equipe_1, Equipe):
            raise TypeError("L'equipe doit être une instance de Equipe")
        if not isinstance(equipe_2, Equipe):
            raise TypeError("L'equipe doit être une instance de Equipe")
        if not isinstance(historique, Historique):
            raise TypeError("L'historique doit être une instance"
                            "de Historique")
        self.equipe_1 = equipe_1
        self.equipe_2 = equipe_2
        self.historique = historique

    def methode_simple(self) -> Equipe:
        '''Cette méthode prédit l'issue d'un match en regardant simplement
        qui des deux équipes a gagné plus de fois contre l'autre

        Returns:
            Equipe: l'equipe qui a gagné plus de fois
        '''

        hist = self.historique.get_historique(matches, self.equipe_1,
                                              self.equipe_2)
        nb_match = len(hist)
        vic_eq_1 = 0
        vic_eq_2 = 0
        for i in range(nb_match):
            if hist[i].gagnant() == self.equipe_1:
                vic_eq_1 += 1
            if hist[i].gagnant() == self.equipe_2:
                vic_eq_2 += 1
        if vic_eq_1 > vic_eq_2:
            return self.equipe_1
        if vic_eq_1 == vic_eq_2:
            return "egalite"
        return self.equipe_2

    def methode_pondere(self):
        vic_eq_1 = 0
        vic_eq_2 = 0
        hist = self.historique.get_historique(matches, self.equipe_1,
                                              self.equipe_2)
        for i in range(len(hist)):
            if hist[i].date.year >= self.historique.__date_fin.year - 4:
                if hist[i].gagnant() == self.equipe_1:
                    vic_eq_1 += 4
                if hist[i].gagnant() == self.equipe_2:
                    vic_eq_2 += 4
            if hist[i].date.year <= self.historique.__date_debut.year + 4:
                if hist[i].gagnant() == self.equipe_1:
                    vic_eq_1 += 1
                if hist[i].gagnant() == self.equipe_2:
                    vic_eq_2 += 1
            else:
                if hist[i].gagnant() == self.equipe_1:
                    vic_eq_1 += 2
                if hist[i].gagnant() == self.equipe_2:
                    vic_eq_2 += 2
        if vic_eq_1 > vic_eq_2:
            return self.equipe_1
        if vic_eq_1 == vic_eq_2:
            return "egalite"
        return self.equipe_2

    def methode_recente(self):
        vic_eq_1 = 0
        vic_eq_2 = 0
        hist1 = self.historique.get_historique_seul(matches, self.equipe_1)
        hist2 = self.historique.get_historique_seul(matches, self.equipe_2)
        for i in range(len(hist1)):
            if hist1[i].gagnant() == self.equipe_1:
                vic_eq_1 += 1
        for i in range(len(hist2)):
            if hist2[i].gagnant() == self.equipe_2:
                vic_eq_2 += 1
        if vic_eq_1 > vic_eq_2:
            return self.equipe_1
        if vic_eq_1 == vic_eq_2:
            return "egalite"
        return self.equipe_2
