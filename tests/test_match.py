from project.match import Match
import re
import pytest
import pandas as pd
df = pd.read_csv("./BDD/results.csv")


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'equipe_dom': "France"}, TypeError, (
            "L'equipe doit être une instance de Equipe"
        )),
        ({'equipe_ext': ["Germany"]}, TypeError, (
            "L'equipe doit être une instance de Equipe"
        )),
        ({'tournoi': ["World Cup"]}, TypeError, (
            "Le tournoi doit être une instance de str"
        ))
    ]
)
def test_match_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Match(**kwargs)
