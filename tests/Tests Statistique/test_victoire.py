from project.Statistique.victoire import Victoire
import pandas as pd
import pytest
import re


df = pd.read_csv('./BDD/results.csv')


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'date_debut': "2022"}, TypeError, (
            "La date de debut doit être une instance de date")),
        ({'date_fin': "2022"}, TypeError, (
            "La date de fin doit être une instance de date"
        ))
    ]
)
def test_victoire_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Victoire(**kwargs)
