from project.historique import Historique
import pytest
import re


@pytest.mark.parametrize(
    'kwargs, erreur, erreur_message',
    [
        ({'date_debut': "2022"}, TypeError, (
            "La date de debut doit être une instance de date"
        )),
        ({'date_fin': "2022"}, TypeError, (
            "La date de fin doit être une instance de date"
        ))
    ]
)
def test_historique_init_echec(kwargs, erreur, erreur_message):
    with pytest.raises(erreur, match=re.escape(erreur_message)):
        Historique(**kwargs)
