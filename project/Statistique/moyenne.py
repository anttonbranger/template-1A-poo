from project.Statistique.statistique import Statistique
from abc import ABC
from datetime import date
from project.equipe import Equipe


class Moyenne(Statistique, ABC):
    '''Classe abstraite qui code sans l'implémenter la methode moyenne'''
    def __init__(self, date_debut: date, date_fin: date):
        self.date_debut = date_debut
        self.date_fin = date_fin

    def moyenne(self, equipe: Equipe) -> float:
        pass
