class Score():

    def __init__(self, score_dom: int, score_ext: int):
        self.score_dom = score_dom
        self.score_ext = score_ext
        if not isinstance(score_dom, int):
            raise TypeError("Le score_dom doit être une instance de int")
        if not isinstance(score_ext, int):
            raise TypeError("Le score_ext doit être une instance de int")

    def __repr__(self) -> str:
        return f"Score({self.score_dom}, {self.score_ext})"
